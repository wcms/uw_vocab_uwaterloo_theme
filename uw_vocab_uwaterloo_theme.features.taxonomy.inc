<?php

/**
 * @file
 * uw_vocab_uwaterloo_theme.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function uw_vocab_uwaterloo_theme_taxonomy_default_vocabularies() {
  return array(
    'uwaterloo_theme' => array(
      'name' => 'uWaterloo Theme',
      'machine_name' => 'uwaterloo_theme',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
