<?php

/**
 * @file
 * uw_vocab_uwaterloo_theme.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function uw_vocab_uwaterloo_theme_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Robust employer-employee relationship',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'robust_employer_employee_relationship',
    'uuid' => '2f99d1c2-af66-43a9-b04d-75694a46516a',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Robust employer-employee relationship',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Robust employer-employee relationship',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Innovation',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'innovation',
    'uuid' => '318f49aa-651d-4e4b-8fcb-3fe378c3a69a',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Innovation',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Innovation',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Outstanding academic programming',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'outstanding_academic_programming',
    'uuid' => '61766bd9-c0db-43a6-957b-22841c72d79f',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Outstanding academic programming',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Outstanding academic programming',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Experiential education',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'experiential_education',
    'uuid' => '833e99fb-6ead-49b9-afc6-fe3cc98c3e20',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Experiential education',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Experiential education',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Vibrant student experience',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'vibrant_student_experiencce',
    'uuid' => 'a6c4af7f-f2f4-4713-9d01-7d990d9d4da1',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Vibrant student experience',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Vibrant student experience',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Global outlook',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'global_outlook',
    'uuid' => 'b62d9514-a166-462c-a61e-89f4a2fa6433',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Global outlook',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Global outlook',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Transformational research',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'transformational_research',
    'uuid' => 'dd32163c-5e9d-4abd-9f0e-b8cc0d93cf9f',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Transformational research',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Transformational research',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Sound value system',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'sound_value_system',
    'uuid' => 'de6a4263-8bd6-4030-b954-9723aeec373e',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Sound value system',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Sound value system',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Entrepreneurship',
    'description' => '',
    'format' => 'uw_tf_standard',
    'weight' => 0,
    'machine_name' => 'entrepreneurship',
    'uuid' => 'f3be327b-265b-4d2f-86f4-e618ef91a914',
    'vocabulary_machine_name' => 'uwaterloo_theme',
    'name_field' => array(
      'en' => array(
        0 => array(
          'value' => 'Entrepreneurship',
          'format' => NULL,
        ),
      ),
      'und' => array(
        0 => array(
          'value' => 'Entrepreneurship',
          'format' => NULL,
        ),
      ),
    ),
    'description_field' => array(
      'und' => array(
        0 => array(
          'value' => '',
          'summary' => NULL,
          'format' => 'uw_tf_standard',
          'safe_summary' => '',
        ),
      ),
    ),
    'path' => array(
      'pathauto' => 1,
    ),
  );
  return $terms;
}
