<?php

/**
 * @file
 * uw_vocab_uwaterloo_theme.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function uw_vocab_uwaterloo_theme_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'metatag_enable_taxonomy_term__uwaterloo_theme';
  $strongarm->value = TRUE;
  $export['metatag_enable_taxonomy_term__uwaterloo_theme'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_taxonomy_term_uwaterloo_theme_pattern';
  $strongarm->value = 'themes/[term:machine-name]';
  $export['pathauto_taxonomy_term_uwaterloo_theme_pattern'] = $strongarm;

  return $export;
}
