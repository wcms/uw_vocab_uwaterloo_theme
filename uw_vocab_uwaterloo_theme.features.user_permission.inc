<?php

/**
 * @file
 * uw_vocab_uwaterloo_theme.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_vocab_uwaterloo_theme_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'add terms in uwaterloo_theme'.
  $permissions['add terms in uwaterloo_theme'] = array(
    'name' => 'add terms in uwaterloo_theme',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy_access_fix',
  );

  // Exported permission: 'delete terms in uwaterloo_theme'.
  $permissions['delete terms in uwaterloo_theme'] = array(
    'name' => 'delete terms in uwaterloo_theme',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  // Exported permission: 'edit terms in uwaterloo_theme'.
  $permissions['edit terms in uwaterloo_theme'] = array(
    'name' => 'edit terms in uwaterloo_theme',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'taxonomy',
  );

  return $permissions;
}
